/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chekersv2;




import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Krzysiek
 */
public class Board extends Pane{

    private ArrayList<Rectangle> boardImg = new ArrayList<>();
    private ArrayList<Pawn> pawnsImg = new ArrayList<>();
    private Pawn pawnToDragg;
    private Color turn;
    private Integer[][] board = {{0,2,0,2,0,2,0,2},
                             {2,0,2,0,2,0,2,0},
                             {0,2,0,2,0,2,0,2},
                             {1,0,1,0,1,0,1,0},
                             {0,1,0,1,0,1,0,1},
                             {3,0,3,0,3,0,3,0},
                             {0,3,0,3,0,3,0,3},
                             {3,0,3,0,3,0,3,0}
                            };
    Board(){
        turn = Color.RED;
        for(int i = 0; i<8; i++){
            for(int j = 0; j<8; j++){
                Rectangle rec = new Rectangle();
                rec.setX(j*75);
                rec.setY(i*75);
                rec.setWidth(80);
                rec.setHeight(80);
                if((j+i)%2==0){
                    rec.setFill(Color.WHITE);
                }else{
                    rec.setFill(Color.BLACK);
                }
                if((i<3||i>4) && (j+i)%2==1){
                    Pawn pawn = new Pawn();
                    pawn.setRadius(37.5);
                    if(i<3) pawn.setColor(Color.RED);
                    else pawn.setColor(Color.GOLD);
                    pawn.setCenterPosition(j*75+37.5, i*75+37.5);
                    pawnsImg.add(pawn);
                }
                boardImg.add(rec);
            }
           
           
        }
       boardImg.stream().forEach((cell)->{
         getChildren().add(cell);
       });
        pawnsImg.stream().forEach((pawn)->{
            mouseEvents(pawn);
            getChildren().add(pawn);
        });

    }
    
    private void mouseEvents(Pawn pawn){
        Position lastMousePosition = new Position();
        Position firstPosition = new Position();
        
        pawn.addEventFilter(MouseEvent.MOUSE_PRESSED, (
                MouseEvent event)->{
           
            pawnToDragg = pawn;
            
            if(pawn.getFill() == turn ){
                pawnToDragg.checkDraggable(board);
                pawnToDragg.setFirstPosition();
                pawnToDragg.setLastPosition(event.getSceneX(), event.getSceneY());
                prepareToDrag(event, firstPosition, lastMousePosition, pawn);
            }
            
        });
        
        pawn.addEventFilter(MouseEvent.MOUSE_DRAGGED, (
                MouseEvent event)->{
            if(pawn.getFill() == turn){
                pawnToDragg.setLastPosition(event.getSceneX(), event.getSceneY());
                draggCircle(event, lastMousePosition);
                getMousePosition(event, lastMousePosition);
            }
        });
        
        pawn.addEventFilter(MouseEvent.MOUSE_RELEASED, (
                MouseEvent event)->{
            if(pawn.getFill() == turn){
                boolean feedback = pawnToDragg.move(board);
                jumpOver(pawnToDragg);
                changeTurn(feedback);
                checkIfQueen(pawnToDragg);
                
            }
        });
        
    }
    
    private void getMousePosition(MouseEvent event, Position pos){
        pos.setPosition(event.getSceneX(), event.getSceneY());
    }
    private void prepareToDrag(MouseEvent event, Position first,
            Position last, Pawn pawn)
    {
        if(pawnToDragg.getIsDraggable()){
            getMousePosition(event, last);
            first.setPosition(pawn.getCenterX(), pawn.getCenterY());
        }
    }
    private void draggCircle(MouseEvent event, Position pos){
        if(pawnToDragg.getIsDraggable()){
            double deltaX = event.getSceneX() - pos.getX();
            double deltaY = event.getSceneY() - pos.getY();
            pawnToDragg.setCenterX(pos.getX() - deltaX);
            pawnToDragg.setCenterY(pos.getY() - deltaY);
        }
    }   
    
    private void jumpOver(Pawn pawn){
        if(pawn.getIsAttack()){
            double deltaX = pawn.getFirstPosition().getX() -
                            pawn.getLastPosition().getX();
            double deltaY = pawn.getFirstPosition().getY() -
                            pawn.getLastPosition().getY();
            int offsetIndex = deltaY<0 ? 1 : -1;
            if(deltaX<0){
                removePawnFromBoard(pawn.getRow()-offsetIndex, pawn.getColumn()-1);
            }else{
                removePawnFromBoard(pawn.getRow()-offsetIndex, pawn.getColumn()+1);
            }
            pawn.setAttack(false);
        }
    }
    
    private void removePawnFromBoard(int row, int col){
        for(int i = 0; i<pawnsImg.size(); i++){
            if(pawnsImg.get(i).getRow() == row &&
                   pawnsImg.get(i).getColumn() == col){
                                pawnsImg.get(i).setCenterPosition(-500, -500);
            }
        }
    }
    
    private void changeTurn(boolean feedback){
        if(feedback){
            if(turn == Color.RED){
                turn = Color.GOLD;
            }else{
                turn = Color.RED;
            }
        }
    }
    
    private void checkIfQueen(Pawn pawn){
        if(pawn instanceof Pawn && pawn.getIsQueen()){
                    Queen queen = new Queen(pawn);
                    getChildren().remove(pawn);
                    pawnsImg.remove(pawn);
                    pawnsImg.add(queen);
                    mouseEvents(queen);
                    getChildren().add(queen);
                    requestLayout();
                 }
    }
    
    private void showBoard(){
        for(Integer[] i: board){
            for(Integer x: i){
                System.out.print(x+" ");
            }
            System.out.println("");
        }
        System.out.println("");
    }
    
     public void refresh() {
        board = new Integer[][]{{0,2,0,2,0,2,0,2},
                                {2,0,2,0,2,0,2,0},
                                {0,2,0,2,0,2,0,2},
                                {1,0,1,0,1,0,1,0},
                                {0,1,0,1,0,1,0,1},
                                {3,0,3,0,3,0,3,0},
                                {0,3,0,3,0,3,0,3},
                                {3,0,3,0,3,0,3,0}
                              };
        getChildren().removeAll(pawnsImg);
        pawnsImg.clear();
        for(int i = 0; i<8; i++){
            for(int j = 0; j<8; j++){
                if((i<3||i>4) && (j+i)%2==1){
                    Pawn pawn = new Pawn();
                    pawn.setRadius(37.5);
                    if(i<3) pawn.setColor(Color.RED);
                    else pawn.setColor(Color.GOLD);
                    pawn.setCenterPosition(j*75+37.5, i*75+37.5);
                    pawnsImg.add(pawn);
                }    
            }
        }
        pawnsImg.stream().forEach((pawn)->{
            getChildren().add(pawn);
            mouseEvents(pawn);
        });
        turn = Color.RED;
     }
     
     
     public Color getTurn(){
         return turn;
     }

    
}


