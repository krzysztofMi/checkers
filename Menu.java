/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chekersv2;



import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 *
 * @author Krzysiek
 */
public class Menu extends VBox{
    private Button startButton;
    private Button exitButton;
    private Rectangle turn;
    private Text text;
    private Board board;
    public Menu(Board board){
        super(50);
        this.board = board;
        Font font = new Font("Batang", 30);
        startButton = new Button("Start");
        exitButton = new Button("Exit");
        startButton.setMinSize(200, 100);
        exitButton.setMinSize(200, 100);
        startButton.setFont(font);
        exitButton.setFont(font);
        turn = new Rectangle();
        turn.setFill(Color.RED);
        turn.setWidth(100);
        turn.setHeight(100);
        
        text = new Text("Turn");
        text.setFont(font);
        this.setAlignment(Pos.CENTER);
        
        getChildren().add(text);
        getChildren().add(turn);
        getChildren().add(startButton);
        getChildren().add(exitButton);
        
        
        exitButton.setOnAction(exitAction);
        startButton.setOnAction(e->board.refresh());
        
                
        Timeline timeline = new Timeline(new KeyFrame(
        Duration.millis(10),
        ae -> setTurn(board.getTurn())));

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        
            
        
    }
    
    EventHandler<ActionEvent> exitAction = new EventHandler<ActionEvent>(){

        @Override
        public void handle(ActionEvent event) {
           Platform.exit();
        }
        
    };
    
    public void setTurn(Color color){
        turn.setFill(color);
    }
}
