
package chekersv2;

import javafx.scene.paint.Color;



public class Queen extends Pawn{
   private int offsetIndex;



   public Queen(Pawn pawn) {
       super(pawn);
       this.setStroke(Color.BLUEVIOLET);
       this.setStrokeWidth(10);
    }
   
   @Override
   public void checkDraggable(Integer[][] board){ 
        isDraggable = false;
        if(checkIfCanMove(board)){
            isDraggable = true;
        }
        
        if(checkIfCanJump(board)){
            isDraggable = true;
        }
    }
   
   
   @Override
   protected boolean checkIfCanMove(Integer[][] board){
        isMoveLeft = false;
        if( column!=0 && ((row!=7 && (board[row+1][column-1] == 1)) ||
                         (row!=0 && board[row-1][column-1] == 1))){
            isMoveLeft = true;
        }
        isMoveRight = false;
        if( column!=7 && ((row!=7 && (board[row+1][column+1] == 1)) ||
           (row!=0 && board[row-1][column+1] == 1))){
            isMoveRight = true;
        }
        return (isMoveLeft || isMoveRight);
    }
    
   @Override
    protected boolean checkIfCanJump(Integer[][] board){
        Color color = (Color) getFill();
        isJumpLeft = false;
        int otherPawnNumber = getFill() == Color.RED ? 3:2;
        if(column>1 && ((row>1 &&
                        (board[row-2][column-2] == 1) &&
                        (board[row-1][column-1] == otherPawnNumber)) ||
                (row<6 &&
                        (board[row+2][column-2] == 1) &&
                        (board[row+1][column-1] == otherPawnNumber))))
                
        {
            isJumpLeft = true;  
        }
        isJumpRight = false;
        if(column<6 && ((row>1 &&
                        (board[row-2][column+2] == 1) &&
                        (board[row-1][column+1] == otherPawnNumber)) ||
                (row<6 &&
                        (board[row+2][column+2] == 1) &&
                        (board[row+1][column+1] == otherPawnNumber))))
        {
            isJumpRight = true;
        }
        return (isJumpRight || isJumpLeft);
    }
    
    @Override
    public boolean move(Integer[][] board){
        if(isMoveLeft == false && isMoveRight == false &&
            isJumpLeft == false && isJumpRight == false){
            return dontMove();
        }
        double distanceX = lastPosition.getX() - firstPosition.getX();
        double distanceY = lastPosition.getY() - firstPosition.getY();
        
        offsetIndex = distanceY>0 ? 1:-1;
        if((row == 0 && offsetIndex == -1) ||
            (row == 7 && offsetIndex == 1)){
            return dontMove();
        }
        if(distanceY*offsetIndex<37.5 || distanceY*offsetIndex>187.5 && 
                (isMoveLeft || isMoveRight) && (isJumpLeft || isJumpRight)){
            return dontMove();
        }else
        if(isMoveLeft && (distanceX>-112.5) &&  (distanceX<-37.5) &&
                         (distanceY*offsetIndex<112.5) && 
                         (board[row+offsetIndex][column-1] == 1)){
            moveLeft(board);
        }else 
        if(isMoveRight && (distanceX<112.5) && (distanceX>37.5) &&
                          (distanceY*offsetIndex<112.5)&& 
                          (board[row+offsetIndex][column+1] == 1)){
            moveRight(board);
        }else
        if(isJumpLeft && distanceX>-187.5 && distanceX<-112.5  &&
                (distanceY*offsetIndex>112.5)){
            jumpLeft(board);
             if(checkIfCanJump(board)){
                return false;
            }
        }else
        if(isJumpRight && (distanceX<187.5) && (distanceX>112.5) && 
                          (distanceY*offsetIndex>112.5)){
            jumpRight(board);
            if(checkIfCanJump(board)){
      
                return false;
            }
        }else{
            return dontMove();
        }
        return true;
    }
    
    
   @Override
    protected void moveLeft(Integer[][] board){
        board[row][column] = 1;
        board[row+offsetIndex][column-1] = pawnNumber;
        setCenterPosition(firstPosition.getX()-75, 
        firstPosition.getY() + 75*offsetIndex);
    }
  
   @Override
    protected void moveRight(Integer[][] board){
       board[row][column] = 1;
        board[row+offsetIndex][column+1] = pawnNumber;
        setCenterPosition(firstPosition.getX()+75,
           firstPosition.getY() + 75*offsetIndex);
    }
    
    
   @Override
    protected void jumpRight(Integer[][] board){
        board[row][column] = 1;
            board[row+offsetIndex][column+1] = 1;
            board[row+2*offsetIndex][column+2] = pawnNumber;
            isAttack = true;
            setCenterPosition(firstPosition.getX()+150,
                                   firstPosition.getY()+150*offsetIndex);
    }
    
   @Override
    protected void jumpLeft(Integer[][] board){
        board[row][column] = 1;
        board[row+offsetIndex][column-1] = 1;
        board[row+2*offsetIndex][column-2] = pawnNumber;
        isAttack = true;
        setCenterPosition(firstPosition.getX()-150,
                               firstPosition.getY()+150*offsetIndex);
    }
    
}
