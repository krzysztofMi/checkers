/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chekersv2;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Krzysiek
 */
public class Pawn extends Circle{
    protected boolean isDraggable;
    protected int row, column;
    protected Position firstPosition, lastPosition;
    protected boolean isJumpLeft, isJumpRight;
    protected boolean isMoveLeft, isMoveRight;
    protected boolean isAttack;
    protected int pawnNumber;
    
    private int offsetIndex;
    private boolean isQueen = false;
    
    public Pawn(Pawn p){
        setColor((Color) p.getFill());
        setRadius(p.getRadius());
        setCenterPosition(p.getCenterX(), p.getCenterY());
    }

    Pawn() {
        
    }
    
    public void setColor(Color color){
        setFill(color);
        offsetIndex = getFill() == Color.RED ? 1:-1;
        pawnNumber = offsetIndex == 1 ? 2:3;
    }
    
    
    public void setCenterPosition(double x, double y){
        setCenterX(x);
        setCenterY(y);
        row = (int) (getCenterY()/75-0.5);
        column = (int) (getCenterX()/75-0.5);
        
    }
    

   
    public void setFirstPosition(){
        firstPosition = new Position(getCenterX(), getCenterY());
    }
    
    public void setLastPosition(double x, double y){
        lastPosition = new Position(x, y);
    }
    
    public void setDraggable(boolean set){
        isDraggable = set;
    }
    
    public void setAttack(boolean isAttack){
        this.isAttack = isAttack;
    }
    
    public boolean getIsAttack(){
        return isAttack;
    }
    
    public boolean getIsQueen(){
        return isQueen;
    }
    
    public boolean getIsDraggable(){
        return isDraggable;
    }
    
    public int getRow(){
        return row;
    }
    
    
    public int getColumn(){
        return column;
    }
    
    public Position getFirstPosition(){
        return firstPosition;
    }
    
     public Position getLastPosition(){
        return lastPosition;
    }
    
    public void checkDraggable(Integer[][] board){ 
        isDraggable = false;
        if(checkIfCanMove(board)){
            isDraggable = true;
        }
        
        if(checkIfCanJump(board)){
            isDraggable = true;
        }
    }
    
    protected boolean checkIfCanJump(Integer[][] board){
        Color color = (Color) getFill();
        isJumpLeft = false;
        if(column>1 && (row>1 || color != Color.GOLD) && (row<6 || color != Color.RED) &&
                        (board[row+2*offsetIndex][column-2] == 1) &&
                        (board[row+offsetIndex][column-1] != 1) &&
                        (board[row+offsetIndex][column-1] != pawnNumber))
        {
            isJumpLeft = true;  
        }
        isJumpRight = false;
        if(column<6 && (row>1 || color != Color.GOLD) && (row<6 || color != Color.RED) &&
                        (board[row+2*offsetIndex][column+2] == 1) &&
                        (board[row+offsetIndex][column+1] != 1) &&
                        (board[row+offsetIndex][column+1] != pawnNumber))
        {
            isJumpRight = true;
        }
        return (isJumpRight || isJumpLeft);
    }
    
    protected boolean checkIfCanMove(Integer[][] board){
        isMoveLeft = false;
        if(column!=0 && board[row+offsetIndex][column-1] == 1){
            isMoveLeft = true;
        }
        isMoveRight = false;
        if(column!=7 && board[row+offsetIndex][column+1] == 1){
            isMoveRight = true;
        }
        return (isMoveLeft || isMoveRight);
    }
    
    protected boolean move(Integer[][] board){
        if(isMoveLeft == false && isMoveRight == false &&
            isJumpLeft == false && isJumpRight == false){
            return dontMove();
        }
        double distanceX = lastPosition.getX() - firstPosition.getX();
        double distanceY = lastPosition.getY() - firstPosition.getY();
        if(distanceY*offsetIndex<37.5 || distanceY*offsetIndex>187.5 && 
                (isMoveLeft || isMoveRight) && (isJumpLeft || isJumpRight)){
            return dontMove();
        }else
        if(isMoveLeft && (distanceX>-112.5) &&  (distanceX<-37.5) &&
                         (distanceY*offsetIndex<112.5)){
            moveLeft(board);
        }else 
        if(isMoveRight && (distanceX<112.5) && (distanceX>37.5) &&
                          (distanceY*offsetIndex<112.5)){
            moveRight(board);
        }else
        if(isJumpLeft && distanceX>-187.5 && distanceX<-112.5  &&
                (distanceY*offsetIndex>112.5)){
            jumpLeft(board);
            if(checkIfCanJump(board)){
                checkIfQueen();
                return false;
            }
        }else
        if(isJumpRight && (distanceX<187.5) && (distanceX>112.5) && 
                          (distanceY*offsetIndex>112.5)){
            jumpRight(board);
            if(checkIfCanJump(board)){ 
                checkIfQueen();
                return false;
            }
        }else{
            return dontMove();
        }
        checkIfQueen();
        return true;
    }
    
    protected boolean dontMove(){
        setCenterX(firstPosition.getX());
        setCenterY(firstPosition.getY());
        return false;
    }
    
    protected void moveLeft(Integer[][] board){
        board[row][column] = 1;
        board[row+offsetIndex][column-1] = pawnNumber;
        setCenterPosition(firstPosition.getX()-75, 
            firstPosition.getY() + 75*offsetIndex);
    }
  
    protected void moveRight(Integer[][] board){
        board[row][column] = 1;
        board[row+offsetIndex][column+1] = pawnNumber;
        setCenterPosition(firstPosition.getX()+75,
           firstPosition.getY() + 75*offsetIndex);
    }
    
    
    protected void jumpRight(Integer[][] board){
        board[row][column] = 1;
            board[row+offsetIndex][column+1] = 1;
            board[row+2*offsetIndex][column+2] = pawnNumber;
            isAttack = true;
            setCenterPosition(firstPosition.getX()+150,
                                   firstPosition.getY()+150*offsetIndex);
    }
    
    protected void jumpLeft(Integer[][] board){
        board[row][column] = 1;
        board[row+offsetIndex][column-1] = 1;
        board[row+2*offsetIndex][column-2] = pawnNumber;
        isAttack = true;
        setCenterPosition(firstPosition.getX()-150,
                               firstPosition.getY()+150*offsetIndex);
    }
    
    private void checkIfQueen(){
        if((row == 0 && getFill() == Color.GOLD) 
                || (row ==7 && getFill() == Color.RED)){
            isQueen = true;
        }
    }
}
