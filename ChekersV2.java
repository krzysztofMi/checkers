/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chekersv2;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Krzysiek
 */
public class ChekersV2 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        
        Board board = new Board();
        Menu menu = new Menu(board);
        
        HBox root = new HBox();
        root.getChildren().add(board);
        root.getChildren().add(menu);

        Scene scene = new Scene(root, 800, 600, Color.AQUA);
        
        
        primaryStage.setTitle("Chekers");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(e->Platform.exit());
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
